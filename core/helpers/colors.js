'use strict';

// Check if need enabled colors
if (!['0', 'false'].includes(String(process.env.LOGGER_COLORS).toLowerCase())) {
    // Enabled all colors, because LOGGER_COLORS is enabled (is not, specifically '0' or 'false')
    return require('colors');
}

// Mocked necesary colors, because LOGGER_COLORS is disabled (is '0' or 'false')
const addProperty = function (color) {
    String.prototype.__defineGetter__(color, function () {
        return this;
    });
};

addProperty('red');
addProperty('blue');
addProperty('green');
addProperty('yellow');
addProperty('magenta');
