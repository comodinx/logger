'use strict';

require('./helpers/colors');

const fs = require('fs');
const winston = require('winston');

const level = process.env.LOGGER_LEVEL || 'http';
const silent = ['1', 'true'].includes(String(process.env.LOGGER_SILENT).toLowerCase());
const formatter = process.env.LOGGER_FORMAT || 'combine';
const transporters = (process.env.LOGGER_TRANSPORTS || 'console').split(',');

class Logger {
    constructor (options) {
        const format = formatter !== 'combine' ? winston.format[formatter]() : winston.format.combine(
            winston.format.simple(),
            winston.format.timestamp(),
            winston.format.printf(info => `[${info.timestamp}] - ${info.level.toUpperCase()} - ${info.message}`)
        );
        const transports = [];

        transporters.forEach(transport => {
            // transport values:
            //     console
            //     file
            //     file:<filepath>:<log level>
            //     stream:<filepath>
            //     http:<host>:<port>:<path>:<auth>:<ssl>
            const parts = transport.trim().split(':');

            switch (parts[0].trim()) {
                case 'console':
                    transports.push(new winston.transports.Console({
                        level: parts[1]
                    }));
                    break;

                case 'file':
                    transports.push(new winston.transports.File({
                        filename: parts[1],
                        level: parts[2]
                    }));
                    break;

                case 'stream':
                    transports.push(new winston.transports.Stream({
                        stream: fs.createWriteStream(parts[1])
                    }));
                    break;

                case 'http':
                    transports.push(new winston.transports.Http({
                        host: parts[1],
                        port: parts[2],
                        path: parts[3],
                        auth: parts[4],
                        ssl: parts[5]
                    }));
                    break;
            }
        })

        this.logger = winston.createLogger({ ...{ level, silent, format, transports }, ...(options || {}) });
    }

    error (...messages) {
        this.log('error', ['✘ Ooops...'.red].concat(messages));
    }

    warn (...messages) {
        this.log('warn', ['⚠'.yellow].concat(messages));
    }

    info (...messages) {
        this.log('info', messages);
    }

    title (...messages) {
        this.log('info', ['==========  '].concat(messages).concat(['  ==========']).map(msg => `${msg}`.magenta));
    }

    success (...messages) {
        this.log('info', ['✔'.green].concat(messages));
    }

    step (...messages) {
        this.log('info', ['•'.blue].concat(messages));
    }

    lap (...messages) {
        this.log('info', ['✦'.magenta].concat(messages));
    }

    arrow (...messages) {
        this.log('info', ['➜'.yellow].concat(messages));
    }

    json (...messages) {
        this.log('info', Array.from(arguments).map(msg => JSON.stringify(msg, null, 2)));
    }

    log (method, messages) {
        this.logger[method].apply(this.logger, [Array.from(messages).join(' ')]); // eslint-disable-line no-useless-call
    }

    write (message, encoding) {
        this.log('http', [`${message || ''}`.trim()]);
    }
}

module.exports = Logger;
