# Logger

@comodinx/logger is a Node.js logger helper based on winston.

## Index

* [Download & Install][install].
* [How is it used?][how_is_it_used].
* [Tests][tests].

## Download & Install

### NPM
```bash
    npm install @comodinx/logger
```

### Source code
```bash
$ git clone https://gitlab.com/comodinx/logger.git
$ cd logger
$ npm install
```

## How is it used?

### Configure

| Environment variable | Values                                                               | Type            | Default value |
|:---------------------|:---------------------------------------------------------------------|:----------------|:--------------|
| LOGGER_ENABLED       | true/false                                                           | boolean         | `true`        |
| LOGGER_SILENT        | true/false                                                           | boolean         | `false`       |
| LOGGER_LEVEL         | http,info,warn or error                                              | string          | http          |
| LOGGER_FORMAT        | simple,json or combine                                               | string          | combine       |
| LOGGER_TRANSPORTS    | console,                                                             | string list (,) | console       |
|                      | file,                                                                |                 |               |
|                      | file:&lt;filepath&gt;:&lt;log level&gt;,                             |                 |               |
|                      | stream:&lt;filepath&gt;,                                             |                 |               |
|                      | http:&lt;host&gt;:&lt;port&gt;:&lt;path&gt;:&lt;auth&gt;:&lt;ssl&gt; |                 |               |

Examples,
```shell
LOGGER_TRANSPORTS=console,file:.access.log,file:.errors.log:error
```

### Logger

```js
const logger = require('@comodinx/logger');

logger.error(new Error('Not Found')); // [2020-01-28T17:16:50.379Z] - ERROR - ✘ Ooops... Error: Not Found
logger.error('This is an error');     // [2020-01-28T17:16:50.379Z] - ERROR - ✘ Ooops... This is an error
logger.warn('This is a warning');     // [2020-01-28T17:16:50.381Z] - WARN - ⚠ This is a warning
logger.info('Hello World!');          // [2020-01-28T17:16:50.381Z] - INFO - Hello World!
logger.title('Hello World!');         // [2020-01-28T17:16:50.382Z] - INFO - ==========   Hello World!   ==========
logger.success('Hello World!');       // [2020-01-28T17:16:50.383Z] - INFO - ✔ Hello World!
logger.arrow('Hello World!');         // [2020-01-28T17:16:50.384Z] - INFO - • Hello World!
logger.step('Hello World!');          // [2020-01-28T17:16:50.384Z] - INFO - ✦ Hello World!
logger.lap('Hello World!');           // [2020-01-28T17:16:50.384Z] - INFO - ➜ Hello World!
```

## Tests

In order to see more concrete examples, **I INVITE YOU TO LOOK AT THE TESTS :)**

### Run the unit tests
```sh
npm test
```

<!-- deep links -->
[install]: #download--install
[how_is_it_used]: #how-is-it-used
[tests]: #tests
